import pandas as pd


class ItemController:
    def __init__(self, rented_items_ids=None):
        self.items = pd.read_csv("data_sets/items.csv")
        self.ids = [] if rented_items_ids is None else rented_items_ids

    def get_random_item(self):
        if len(self.ids) == len(self.items):
            return {}
        item = self.items[~self.items.item_id.isin(self.ids)].sample(1)
        self.ids.append(item.loc[:, ['item_id']].iloc[0, 0])
        return item.to_dict(orient='record')[0]
