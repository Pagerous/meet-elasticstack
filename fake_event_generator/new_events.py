import argparse
import sys
sys.path.append("..")
from fake_event_generator.event_generator import EventGenerator

parser = argparse.ArgumentParser(description='Generate some events.')
parser.add_argument('-m', '--meetings', help='number of meetings to generate', type=int, default=0)
parser.add_argument('-a', '--appraisals', help='number of appraisals to generate', type=int, default=0)
parser.add_argument('-r', '--resignations', help='number of resignations to generate', type=int, default=0)
parser.add_argument('-p', '--projects', help='number of projects to open', type=int, default=0)
parser.add_argument('-c', '--close', help='number of projects to close (if possible)', type=int, default=0)
parser.add_argument('-g', '--get-item', help='number of items to rent', type=int, default=0)
parser.add_argument('-b', '--back-item', help='number of items to return back (if possible)', type=int, default=0)
args = parser.parse_args()

eg = EventGenerator()
eg.event_scheduler({
    'meetings': args.meetings,
    'appraisals': args.appraisals,
    'resignations': args.resignations,
    'projects': args.projects,
    'close': args.close,
    'get_item': args.get_item,
    'back_item': args.back_item
})