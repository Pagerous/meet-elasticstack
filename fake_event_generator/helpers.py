def open_stats():
    with open('stats.txt', 'r') as f:
        final_dict = {}
        for line in f.readlines():
            div_line = line.split("=")
            operation = str if div_line[0] == 'last_fake_time' else int
            final_dict[div_line[0]] = operation(div_line[1].strip())
        return final_dict


def save_stat(stat, value):
    with open('stats.txt', 'r') as f:
        new_lines = [line if line.split("=")[0] != stat else stat + "=" + str(value) + "\n" for line in f.readlines()]
    with open('stats.txt', 'w') as f:
        for line in new_lines:
            f.write(line)
