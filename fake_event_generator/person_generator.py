import numpy as np
import pandas as pd


class PersonGenerator:
    def __init__(self, ids=None, phones_numbers=None, e_mails=None):
        self.men_names = pd.read_csv("data_sets/polish_men_names.csv")
        self.men_surnames = pd.read_csv("data_sets/polish_men_surnames.csv")
        self.women_names = pd.read_csv("data_sets/polish_women_names.csv")
        self.women_surnames = pd.read_csv("data_sets/polish_women_surnames.csv")
        self.fields = pd.read_csv("data_sets/fields.csv")
        self.abilities = pd.read_csv("data_sets/abilities.csv")
        self.ids = [] if ids is None else ids
        self.phones_numbers = [] if phones_numbers is None else phones_numbers
        self.e_mails = [] if e_mails is None else e_mails

    def group_of_students(self, attributes=None, random=False, count=10):
        if attributes is None and random:
            attributes = [[np.random.choice(['M', 'F']), 'Obserwator'] for _ in range(count)]
        return [self.new_person(*attributes[i]) for i in range(len(attributes))]

    def generate_phone_number(self):
        phone_number = str(np.random.randint(low=5e8, high=9e8))
        while phone_number in self.phones_numbers:
            phone_number = str(np.random.randint(low=5e8, high=9e8))
        self.phones_numbers.append(phone_number)
        return phone_number

    def generate_name(self, gender):
        if gender == 'M':
            name = self.men_names.sample(1).iat[0, 0]
            surname = self.men_surnames.sample(1).iat[0, 0]
        elif gender == 'F':
            name = self.women_names.sample(1).iat[0, 0]
            surname = self.women_surnames.sample(1).iat[0, 0]
        else:
            raise ValueError(f'Invalid argument: {gender}')
        return {'name': name, 'surname': surname, 'gender': gender}

    def generate_id(self, year_of_study):
        id_ = self._generate_id(year_of_study)
        while id_ in self.ids:
            id_ = self._generate_id(year_of_study)
        self.ids.append(id_)
        return id_

    def _generate_id(self, year_of_study):
        if year_of_study == "1":
            id_ = str(np.random.randint(low=5e5, high=6e5))
        elif year_of_study == "2":
            id_ = str(np.random.randint(low=4e5, high=5e5))
        elif year_of_study == "3":
            id_ = str(np.random.randint(low=3e5, high=4e5))
        elif year_of_study == "4":
            id_ = str(np.random.randint(low=2e5, high=3e5))
        elif year_of_study == "5":
            id_ = str(np.random.randint(low=1e5, high=2e5))
        else:
            raise ValueError("wrong year of study")
        return id_

    def new_person(self, gender, title):
        name = self.generate_name(gender)
        e_mail = (name['name'] + name['surname'].replace(" ", ".")).lower() + "@gmail.com"
        phone_number = self.generate_phone_number()
        year_of_study = str(np.random.randint(low=1, high=6))
        field_of_study = self.fields.sample(1).iat[0, 0]
        id_ = self.generate_id(year_of_study)

        personal_abilities = self.abilities.sample(np.random.randint(low=1, high=11)).reset_index(drop=True)
        abilities_scores = personal_abilities.loc[:, ['ability']].applymap(lambda x: np.random.randint(low=1, high=7))
        abilities_scores.columns = ['score']
        personal_abilities = pd.concat([personal_abilities, abilities_scores], axis=1).to_dict(orient='records')

        return {'id': id_,
                'student': name,
                'title': title,
                'e_mail': e_mail,
                'phone_number': phone_number,
                'year_of_study': year_of_study,
                'field_of_study': field_of_study,
                'abilities': personal_abilities}


if __name__ == "__main__":
    pg = PersonGenerator()
    data = pg.group_of_students()
    print(data)
