import random
import logging
import time
from datetime import timedelta, datetime
from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk
from .item_controller import ItemController
from .person_generator import PersonGenerator
import fake_event_generator.helpers as help_me


logging.basicConfig(format="%(message)s")


class EventGenerator:
    def __init__(self):
        self.es = Elasticsearch()
        org = self.es.search(index='organization',
                             size=10000,
                             filter_path=['hits.hits._source.id',
                                          'hits.hits._source.e_mail',
                                          'hits.hits._source.phone_number',
                                          'hits.hits._source.item_id',
                                          'hits.hits._source.join_field'])
        self.person_generator = PersonGenerator(ids=[hit['_source']['id']
                                                     for hit in org['hits']['hits']
                                                     if hit['_source']['join_field'] == 'person'],
                                                e_mails=[hit['_source']['e_mail']
                                                         for hit in org['hits']['hits']
                                                         if hit['_source']['join_field'] == 'person'],
                                                phones_numbers=[hit['_source']['phone_number']
                                                                for hit in org['hits']['hits']
                                                                if hit['_source']['join_field'] == 'person'])
        self.items = ItemController(rented_items_ids=[hit['_source']['item_id'] for hit in org['hits']['hits']
                                                      if type(hit['_source']['join_field']) == dict and
                                                      hit['_source']['join_field']['name'] == 'item'])
        self.date_format = "%d/%m/%Y, %H:%M"
        self.stats = help_me.open_stats()
        self.logger = logging.getLogger('event_generator')
        self.logger.setLevel(logging.INFO)

    def event_scheduler(self, events):
        all_events = ['m'] * events['meetings'] \
                     + ['a'] * events['appraisals'] \
                     + ['r'] * events['resignations'] \
                     + ['p'] * events['projects'] \
                     + ['c'] * events['close'] \
                     + ['g'] * events['get_item'] \
                     + ['b'] * events['back_item']
        random.shuffle(all_events)
        operations = {
            'm': {
                'bulk': True,
                'event': self.generate_meeting
            },
            'a': {
                'bulk': True,
                'event': self.generate_appraisal
            },
            'r': {
                'bulk': True,
                'event': self.generate_resignation
            },
            'p': {
                'bulk': False,
                'event': self.generate_new_project
            },
            'c': {
                'bulk': False,
                'event': self.generate_close_project
            },
            'g': {
                'bulk': False,
                'event': self.generate_item_rent
            },
            'b': {
                'bulk': False,
                'event': self.generate_item_return
            }
        }
        stats_before = self.stats.copy()
        for event in all_events:
            time.sleep(1)
            if operations[event]['bulk']:
                bulk(self.es, operations[event]['event']())
            else:
                operations[event]['event']()

        if self.stats != stats_before:
            for stat in self.stats:
                help_me.save_stat(stat, self.stats[stat])

    def generate_meeting(self):
        students = self.es.search(index='organization', size=10000, filter_path=['hits.hits._source.title',
                                                                                 'hits.hits._id',
                                                                                 'hits.hits._source.join_field'])
        event_id = str(self.stats['number_of_meetings'])
        self.stats['number_of_meetings'] += 1

        mem_obs = [student for student in students['hits']['hits'] if student['_source']['join_field'] == 'person']
        participants = random.sample(mem_obs, int(len(mem_obs) * random.randint(50, 100) / 100))
        newcomers = self.person_generator.group_of_students(random=True, count=random.randint(1, 11))

        event_time = self.get_random_event_time()
        self.logger.info(f"{event_time} | MEETING NUMBER: {event_id} | "
                         f"PARTICIPANTS: {len(participants) + len(newcomers)} | "
                         f"NEWCOMERS INCLUDED: {len(newcomers)}")

        for participant in participants:
            yield {
                "_index": "organization",
                "_op_type": "update",
                "_id": participant['_id'],
                "doc": {
                    "event_meeting_" + event_id: True
                }
            }
        for newcomer in newcomers:
            yield {
                "_index": "organization",
                "_op_type": "create",
                "event_meeting_" + event_id: True,
                "time": event_time,
                "join_field": "person",
                **newcomer
            }

    def generate_appraisal(self):
        meetings_participants = self.es.search(index='organization',
                                               size=10000,
                                               filter_path=['hits.hits._id',
                                                            'hits.hits._source.event_meeting_*',
                                                            'hits.hits._source.join_field'])
        for participant in meetings_participants['hits']['hits']:
            if participant['_source']['join_field'] == 'person':
                person = self.es.get(index='organization', id=participant['_id'])
                no_meetings = len(participant.get("_source", []))
                if no_meetings >= 3 and person['_source']['title'] == 'Obserwator':
                    event_time = self.get_random_event_time()
                    name = person['_source']['student']['name'] + " " + person['_source']['student']['surname']
                    self.logger.info(f"{event_time} | APPRAISAL | "
                                     f"{name} took part in {no_meetings} meetings | "
                                     f"Has become member")
                    yield {
                        "_index": "organization",
                        "_op_type": "update",
                        "_id": participant['_id'],
                        "doc": {
                            "title": "Czlonek"
                        }
                    }

    def generate_resignation(self):
        students = self.es.search(index='organization', size=10000, filter_path=['hits.hits._source.title',
                                                                                 'hits.hits._source.student',
                                                                                 'hits.hits._id',
                                                                                 'hits.hits._source.join_field'])
        mem_obs = [student for student in students['hits']['hits'] if student['_source']['join_field'] == 'person'
                   and student['_source']['title'] in ['Czlonek', 'Obserwator']]
        resigners = random.sample(mem_obs, random.randint(1, 9))
        event_time = self.get_random_event_time()
        for resigner in resigners:
            name = resigner['_source']['student']['name'] + " " + resigner['_source']['student']['surname']
            self.logger.info(f"{event_time} | RESIGNATION | "
                             f"{name} with title '{resigner['_source']['title']}' resigned from collaboration")
            self.generate_item_return(parent_id=resigner['_id'])
            yield {
                "_index": "organization",
                "_op_type": "delete",
                "_id": resigner['_id'],
            }

    def generate_new_project(self):
        students = self.es.search(index='organization', size=10000, filter_path=['hits.hits._source.title',
                                                                                 'hits.hits._source.student',
                                                                                 'hits.hits._id',
                                                                                 'hits.hits._source.join_field'])
        members = [student for student in students['hits']['hits']
                   if student['_source']['join_field'] == 'person' and student['_source']['title'] != 'Obserwator']
        project_owner = random.sample(members, 1)[0]
        project_num = self.stats['number_of_projects']
        self.stats['number_of_projects'] += 1
        name = project_owner['_source']['student']['name'] + " " + project_owner['_source']['student']['surname']
        event_time = self.get_random_event_time()
        self.logger.info(f"{event_time} | NEW PROJECT | "
                         f"{name} has started new project '{'super_project_' + str(project_num)}'")
        self.es.index(
            index='organization',
            routing=1,
            body={
                "join_field": {
                    "name": "project",
                    "parent": project_owner['_id']
                },
                "project_name": "super_project_" + str(project_num),
                "project_descr": "description of super_project_" + str(project_num),
                "project_budget": random.randint(3, 150) * 100,
                "project_active": True
            }
        )

    def generate_close_project(self):
        organization = self.es.search(index='organization', size=10000, filter_path=['hits.hits._source.project_*',
                                                                                     'hits.hits._id',
                                                                                     'hits.hits._source.join_field'])
        projects = [project for project in organization['hits']['hits']
                    if type(project['_source']['join_field']) == dict and
                    project['_source']['join_field']['name'] == 'project' and
                    project['_source']['project_active']]
        if projects:
            event_time = self.get_random_event_time()
            to_close = random.sample(projects, 1)[0]
            self.logger.info(f"{event_time} | PROJECT CLOSED | "
                             f"Project '{to_close['_source']['project_name']}' has been closed")
            self.es.update(index='organization',
                           body={"doc": {"project_active": False}},
                           id=to_close['_id'])

    def generate_item_rent(self):
        item = self.items.get_random_item()
        if item:
            students = self.es.search(index='organization', size=10000, filter_path=['hits.hits._source.title',
                                                                                     'hits.hits._source.student',
                                                                                     'hits.hits._id',
                                                                                     'hits.hits._source.join_field'])
            allowed_members = [student for student in students['hits']['hits']
                               if student['_source']['join_field'] == 'person' and
                               student['_source']['title'] != 'Obserwator']

            item_holder = random.sample(allowed_members, 1)[0]

            name = item_holder['_source']['student']['name'] + " " + item_holder['_source']['student']['surname']
            event_time = self.get_random_event_time()
            self.logger.info(f"{event_time} | ITEM RENTED | "
                             f"{name} has rented the item {item['item_id']}: {item['item_name']}")
            self.es.index(
                index='organization',
                routing=1,
                body={
                    "join_field": {
                        "name": "item",
                        "parent": item_holder['_id']
                    },
                    "item_id": item['item_id'],
                    "item_name": item['item_name'],
                    "item_category": item['item_category'],
                    "item_descr": item['item_descr'],
                }
            )

    def generate_item_return(self, parent_id=None):
        organization = self.es.search(index='organization', size=10000, filter_path=['hits.hits._source.item_*',
                                                                                     'hits.hits._id',
                                                                                     'hits.hits._source.join_field'])

        def delete_by_parent(item):
            if parent_id and item['_source']['join_field']['parent'] == parent_id or parent_id is None:
                return True
            return False

        items = [item for item in organization['hits']['hits']
                 if type(item['_source']['join_field']) == dict and
                 item['_source']['join_field']['name'] == 'item' and delete_by_parent(item)]

        if items:
            if parent_id:
                to_returns = items
            else:
                to_returns = random.sample(items, 1)
            event_time = self.get_random_event_time()
            for to_return in to_returns:
                    self.es.delete(index='organization',
                                   id=to_return['_id'])
                    self.items.ids.remove(to_return['_source']['item_id'])
                    self.logger.info(f"{event_time} | ITEM RETURNED | "
                                     f"Item {to_return['_source']['item_id']}: {to_return['_source']['item_name']} "
                                     f"has been returned")

    def random_time_after(self, current_time):
        return current_time + timedelta(days=random.randint(0, 3),
                                        hours=random.randint(0, 24),
                                        minutes=random.randint(0, 60))

    def get_random_event_time(self):
        current_time = self.stats['last_fake_time']
        event_time = self.random_time_after(datetime.strptime(current_time, self.date_format)).strftime(self.date_format)
        self.stats['last_fake_time'] = event_time
        return event_time

