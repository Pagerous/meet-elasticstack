This is example of use the ELK (ElasticStack).

# person

**Fields:**

`id` - number of studen's album; type: keyword

`time` - fake time of event appearance; type: date 

`student` - student's name and surname; type: nested

`title` - student's title respected in students organization; type: keyword

`e_mail` - student's e-mail; type: text

`phone_number` - student's phone number; type: text

`year_of_study` - student's year of study; type: keyword

`field_of_study` - student's field of study; type: text, keyword 

`event_<name_of_event>` - a flag which indicates whether as student took part in an event; type: bool

`abilities`- student's abilities and their score; type: nested



# project

In relation with `person` document, which indicates who is a project leader.

**Fields:**

`project_name` - project name; type: text

`project_descr` - project describtion; type: text

`project_budget` - budget of a project; type: float

`project_active` - flag which indicates whether project is still active; type: boolean



# item

In relation with `person` document, which indicates who holds an item.

**Fields:**

`item_id` - item identifier; type: text

`item_name` - item name; type: text, keyword

`item_descr`- item describtion; type: text

`item_category` - category of an item; type: keyword



# inheritance diagram

```mermaid
graph LR
A[person] 
    A --> B[project]
    A --> C[item]
```

# index creation

```elasticsearch
PUT organization
{
	"mappings": {
		"properties": {
			"id": {
				"type": "keyword" 
			},
			"time": {
			    "type": "date",
			    "format": "dd/MM/yyyy, HH:mm"
			},
			"join_field": {
                "type": "join",
                "relations": {
                    "person": ["project", "item"]
                }
            },
            "student": {
            	"type": "nested",
		"include_in_parent": true
            },
            "title": {
            	"type": "keyword"
            },
            "e_mail": {
            	"type": "text"
            },
            "phone_number": {
            	"type": "text"
            },
            "year_of_study": {
            	"type": "keyword"
            },
            "field_of_study": {
            	"type": "keyword"
            },
            "abilities": {
            	"type": "nested",
		"include_in_parent": true
            },
            "project_name": {
            	"type": "text"
            },
            "project_descr": {
            	"type": "text"
            },
            "project_budget": {
            	"type": "float"
            },
            "project_active": {
            	"type": "boolean"
            },
            "item_id": {
            	"type": "keyword"
            },
            "item_name": {
            	"type": "text",
            	"fields": {
            		"raw": {
            			"type": "keyword"
            		}
            	}
            },
            "item_descr": {
            	"type": "text"
            },
            "item_category": {
            	"type": "keyword"
            }
		}
	}
}
```



# logstash file config

```logstash
input {
  file {
    path => "<rest of absolute path here>/meet-elasticstack/fake_event_generator/data_sets/students.json"
    start_position => "beginning"
	sincedb_path => "nul"
    codec => json 
  }
}
filter{
	mutate{
		add_field => {
			"join_field" => "person"
		}
		remove_field => ["host", "path"]
	}
}
output {
  elasticsearch {
    hosts => "http://localhost:9200"
    index => "organization"
  }
  stdout {}
}
```



# how to run it

1. Create index in ElasticSearch using query from above `index creation` header.
2. Load the sample data using LogStash. Sample `logstash.conf` in above`logstash file config` header.
3. Use fake events generator. To do this run `fake_event_generatorr/new_events.py` . 



# fake events

These fake events are generated:

1. **event_meeting** - random percentage (50-100%) of members and observators take part in a meeting and there are few newcomers (1-10).

2. **appraisal** - these observators which take part in at least 3 meeting are become members.

3. **new_project** - randomly taken member starts a new project with a random group of other members.

4. **project_closed** - randomly selected project is closed.

5. **resignation** - randomly taken group of members and obervators resign from collaboration in organization.

6. **item_rent** - random member rent a random item.

7. **item_return** - random item is returned.

   

# events scheduling

Execute `python new_events.py -h` to see details.

